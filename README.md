# zsh-transfer-l0nax.org

transfer.sh zsh ZSH plugin/ script to work with transfer.l0nax.org


## Installation

**zinit:**
```
zinit ice from"gitlab" as"program" pick"bin/"
zinit light l0nax/zsh-transfer-l0nax.org
```


## Commands

### `transfer`

**Usage:**

```
transfer <file|dir>
```
